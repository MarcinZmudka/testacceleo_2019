package TestTransAccel;

class Switch {

	// Atrybuty:
	public DecisionNode base_DecisionNode;
	public String expr;

	// Gettery i Settery:
	public DecisionNode getBase_DecisionNode() { return base_DecisionNode; }
	public void setBase_DecisionNode(DecisionNode base_DecisionNode) { this.base_DecisionNode = base_DecisionNode; }
	public String getExpr() { return expr; }
	public void setExpr(String expr) { this.expr = expr; }

	// Operacje:

}
